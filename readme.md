# Time-frequency analyses

## Low-frequency alpha and theta power

We estimated low-frequency power via a seven-cycle wavelet transform, applied at linearly spaced center frequencies in 1 Hz steps from 2 to 15 Hz. The step size of estimates was 50 ms, ranging from −1.5 s prior to cue onset to 3.5 s following stimulus offset. Estimates were log10- transformed at the single trial level84, with no explicit baseline.

## Multivariate assessment of spectral power changes with stimulus onset and uncertainty.

To determine changes in spectral power upon stimulus onset, and during stimulus presentation with load, we entered individual power values into multivariate partial least-squares (PLS) analyses (see section “Multivariate partial leastsquares analyses”) using the MEG-PLS toolbox v2.02b86. We concatenated low- (2–15 Hz) and high-frequency (45–90 Hz) power matrices to assess joint changes in the PLS models. To examine a multivariate contrast of spectral changes upon stimulus onset (averaged across conditions) with spectral power in the pre-stimulus baseline period, we performed a task PLS on data ranging from 500 ms pre-stim to 500 ms post-stim. Temporal averages from −700 to −100 ms pre-stimulus onset were subtracted as a baseline. To assess power changes as a function of probe uncertainty, we segmented the data from 500 ms post stim onset to stimulus offset (to exclude transient evoked onset responses) and calculated a task PLS concerning the relation between experimental uncertainty conditions and time–space–frequency power values. As a control, we performed a behavioral PLS analysis to assess the relevance of individual frequency contributions to the behavioral relation. For this analysis, we computed linear slopes relating power to target load for each time–frequency point at the first (within-subject) level, which were subsequently entered into the second-level PLS analysis. On the behavioral side, we assessed both linear changes in pupil diameter, as well as drift rates in the single-target condition and linear decreases in drift rate under uncertainty. Finally, spontaneous fluctuations in pre-stimulus power have been linked to fluctuations in cortical excitability87,88. We thus probed the role of upcoming processing requirements on pre-stimulus oscillations, as well as the potential relation to behavioral outcomes using task and behavioral PLS analyses. The analysis was performed as described above but was restricted to time points occurring during the final second prior to stimulus onset.

This file provides an overview of the analyses contained in this directory.

**Overview:** Calculation of time-frequency power, statistical modelling via multivariate PLS

**a01_compute_tfr**

- calculate theta-to-beta power for individual subjects
- CSD transform
- 7-cycle wavelet (2 to 25 Hz, -1.5s precue to +3.5s post-stimulus)
- create load conditions
- single-trial log10 transform
- average across trials

**b01_TimeOverview_gamma**

- plot temporal traces for gamma

younger adults:
![figure](figures/b01_gammaTraces_ya.png)

older adults:
![figure](figures/b01_gammaTraces_oa.png)

**b01_TimeOverview_theta_alpha**

- plot temporal traces for theta and alpha

younger adults:
![figure](figures/b01_thetaTraces_ya.png)

older adults:
![figure](figures/b01_thetaTraces_oa.png)

younger adults:
![figure](figures/b01_alphaTraces_ya.png)

older adults:
![figure](figures/b01_alphaTraces_oa.png)


**taskPLS_avg**

- Includes signature-specific pls models
- for all models, data were z-scored across conditions, which reduces brainscore outliers

**c01_taskPLS_alpha_stim**

Topography:
![figure](figures/c02_pls_topo_alpha_stim.png)

RCP:
![figure](figures/c02_pls_rcp_alpha_stim.png)

**c02_taskPLS_theta_stim**

Topography:
![figure](figures/c02_pls_topo_theta_stim.png)

RCP:
![figure](figures/c02_pls_rcp_theta_stim.png)

**c03_taskPLS_gamma_stim**

Topography:
![figure](figures/c02_pls_topo_gamma_stim.png)

RCP:
![figure](figures/c02_pls_rcp_gamma_stim.png)

**c04_taskPLS_alpha_prestim**

Topography:
![figure](figures/c04_pls_topo_pre_alpha_stim.png)

RCP:
![figure](figures/c04_pls_rcp_pre_alpha_stim.png)

### K14_taskPLS_load_YAOA_2group_TFR_aperiodic_pupil

- 2 group PLS model
- log10 theta and alpha
- single-trial normalized gamma (90+ Hz)
- sample entropy (scale 1 MSE), 1/f slopes

Given that data types make a differential contribution to the input matrix, brainscores are re-computed post-hoc based on signature-specific brainscores. This happens in script *K14_taskPLS_load_YA_OA_plotLV1_sigspecific*. For some signatures, the channels included in brainscore computation are further restricted: 

- 1/f: posterior channels (44:60)
- SampEn: posterior channels (44:60)
- Alpha-range power: posterior channels (44:60)
- Gamma-range power: posterior channels (44:60)
