clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'RainCloudPlots'));
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'winsor'));

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load(fullfile(pn.data, 'f01_pls.mat'), ...
    'stat', 'result', 'lvdat', 'lv_evt_list')
load(fullfile(pn.data, 'f01_pls_data.mat'), ...
    'datamat_lst', 'TFR_freqs', 'num_chans', 'num_freqs', 'num_time')
load(fullfile(pn.data,'elec.mat'))

%% get IDs

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

%% create signature-specific brainscores
% dot product of signature-specific weights and original data

BrainSalience = result.u(:,1); % get salience for first component
DataX = cat(1,datamat_lst{1},datamat_lst{2}); % sub*cond x chan*freq*num_time
BrainScore = DataX*BrainSalience; %chan*freq*num_time x 1

% sanity-check
BrainScore_orig = result.usc(:,1);
figure; scatter(BrainScore,BrainScore_orig)

% let's reshape both matrices to the chan-freq-time dim

data_orig = reshape(DataX,size(DataX,1), 60, num_freqs, num_time);
salience_orig = reshape(result.u(:,1),num_chans, num_freqs, num_time);

% get only the TFR brainscore

data_TFR = data_orig(:,1:60,1:end-3,:);
data_TFR = reshape(data_TFR,size(data_TFR,1),[]);
salience_TFR = salience_orig(1:60,1:end-3,:);
salience_TFR = reshape(salience_TFR,[],1);
Brainscore_TFR = data_TFR*salience_TFR;

% subdivide into theta, alpha and gamma range

data_TFR = data_orig(:,10:12,TFR_freqs < 8,:);
data_TFR = reshape(data_TFR,size(data_TFR,1),[]);
salience_TFR = salience_orig(10:12,TFR_freqs < 8,:);
salience_TFR = reshape(salience_TFR,[],1);
Brainscore_TFR_theta = data_TFR*salience_TFR;

data_TFR = data_orig(:,44:60,TFR_freqs >= 8 & TFR_freqs <= 25,:);
data_TFR = reshape(data_TFR,size(data_TFR,1),[]);
salience_TFR = salience_orig(44:60,TFR_freqs >= 8 & TFR_freqs <= 25,:);
salience_TFR = reshape(salience_TFR,[],1);
Brainscore_TFR_alpha = data_TFR*salience_TFR;

data_TFR = data_orig(:,44:60,TFR_freqs > 25,:);
data_TFR = reshape(data_TFR,size(data_TFR,1),[]);
salience_TFR = salience_orig(44:60,TFR_freqs > 25,:);
salience_TFR = reshape(salience_TFR,[],1);
Brainscore_TFR_gamma = data_TFR*salience_TFR;

% get only the sampen brainscore

data_sampen = data_orig(:,44:60,end-2,:);
data_sampen = reshape(data_sampen,size(data_TFR,1),[]);
salience_sampen = salience_orig(44:60,end-2,:);
salience_sampen = reshape(salience_sampen,[],1);
Brainscore_sampen = data_sampen*salience_sampen;

% get only the 1/f brainscore

data_1f = data_orig(:,1:60,end-1,1);
data_1f = reshape(data_1f,size(data_TFR,1),[]);
salience_1f = salience_orig(1:60,end-1,1);
salience_1f = reshape(salience_1f,[],1);
Brainscore_1f = data_1f*salience_1f;

% get only the pupil brainscore

data_pupil = data_orig(:,1,end,:);
data_pupil = reshape(data_pupil,size(data_TFR,1),[]);
salience_pupil = salience_orig(1,end,:);
salience_pupil = reshape(salience_pupil,[],1);
Brainscore_pupil = data_pupil*salience_pupil;

groupsizes=sum(result.num_subj_lst);
conditions=lv_evt_list;
conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
condData = []; uData = [];
for indGroup = 1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        BrainScore_TFR(indCond,:) = Brainscore_TFR(targetEntries,1);
        BrainScore_TFR_theta(indCond,:) = Brainscore_TFR_theta(targetEntries,1);
        BrainScore_TFR_alpha(indCond,:) = Brainscore_TFR_alpha(targetEntries,1);
        BrainScore_TFR_gamma(indCond,:) = Brainscore_TFR_gamma(targetEntries,1);
        BrainScore_sampen(indCond,:) = Brainscore_sampen(targetEntries,1);
        BrainScore_1f(indCond,:) = Brainscore_1f(targetEntries,1);
        BrainScore_pupil(indCond,:) = Brainscore_pupil(targetEntries,1);
        BrainScore_all(indCond,:) = BrainScore(targetEntries,1);
    end
end

%% %% plot linear brainscore modulation of different signatures (and intercepts)
% winsorize all data at [2.5 97.5] level

X = [1 1; 1 2; 1 3; 1 4];

% BS.TFR_all = BrainScore_TFR; b=X\BS.TFR_all;
% BS.TFR_all_linear = b(2,:);
% BS.TFR_all_linear = winsor(BS.TFR_all_linear,[2.5 97.5]);
% BS.TFR_all_int = b(1,:);

BS.TFR_theta = BrainScore_TFR_theta; b=X\BS.TFR_theta;
BS.TFR_theta_linear = b(2,:);
BS.TFR_theta_linear = winsor(BS.TFR_theta_linear,[2.5 97.5]);
BS.TFR_theta_int = b(1,:);    

BS.TFR_alpha = BrainScore_TFR_alpha; b=X\BS.TFR_alpha;
BS.TFR_alpha_linear = b(2,:);
BS.TFR_alpha_linear = winsor(BS.TFR_alpha_linear,[2.5 97.5]);
BS.TFR_alpha_int = b(1,:);

BS.TFR_gamma = BrainScore_TFR_gamma; b=X\BS.TFR_gamma;
BS.TFR_gamma_linear = b(2,:);
BS.TFR_gamma_linear = winsor(BS.TFR_gamma_linear,[2.5 97.5]);
BS.TFR_gamma_int = b(1,:);

BS.sampen = BrainScore_sampen; b=X\BS.sampen;
BS.sampen_linear = b(2,:);
BS.sampen_linear = winsor(BS.sampen_linear,[2.5 97.5]);
BS.sampen_int = b(1,:);

BS.OneOverF = BrainScore_1f; b=X\BS.OneOverF;
BS.OneOverF_linear = b(2,:);
BS.OneOverF_linear = winsor(BS.OneOverF_linear,[2.5 97.5]);
BS.OneOverF_int = b(1,:);

BS.pupil = BrainScore_pupil; b=X\BS.pupil;
BS.pupil_linear = b(2,:);
BS.pupil_linear = winsor(BS.pupil_linear,[2.5 97.5]);
BS.pupil_int = b(1,:);

BS.all = BrainScore_all; b=X\BS.all;
BS.all_linear = b(2,:);
BS.all_linear = winsor(BS.all_linear,[2.5 97.5]);
BS.all_int = b(1,:);

BS.all_equalized = cat(1, BS.TFR_theta_linear, ...
BS.TFR_alpha_linear, ...
BS.TFR_gamma_linear, ...
BS.sampen_linear, ...
BS.OneOverF_linear, ...
BS.pupil_linear);
BS.all_equalized = squeeze(nanmean(BS.all_equalized,1));

BS.all_int_equalized = cat(1, BS.TFR_theta_int, ...
    BS.TFR_alpha_int, ...
    BS.TFR_gamma_int, ...
    BS.sampen_int, ...
    BS.OneOverF_int, ...
    BS.pupil_int);
BS.all_int_equalized = squeeze(nanmean(BS.all_int_equalized,1));
% this score only makes sense if it operates on standardized betas
% (not true, because both load levels and brainscores are identical units here)

BS.IDs = IDs;

idx_YA = cellfun(@str2num, IDs)<2000;
idx_OA = cellfun(@str2num, IDs)>2000;

% plot linear loadings by age group with values superimposed

parameters = {'TFR_theta_linear', 'TFR_alpha_linear', 'TFR_gamma_linear',...
    'sampen_linear', 'OneOverF_linear', 'pupil_linear', 'all_linear', 'all_equalized'};
colorm = repmat([.6 .6 .6],8,1);

h = figure('units','normalized','position',[.1 .1 .35 .4]);
set(gcf,'renderer','Painters')
for indParam = 1:8
    subplot(2,4,indParam)
    curParam = BS.(parameters{indParam});
    plot_data{1} = curParam(idx_YA);
    plot_data{2} = curParam(idx_OA);
    cla;
    hold on;
    for indGroup = 1:2
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(indParam,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
        % test deviation from zero
        [h1, p] = ttest(plot_data{indGroup});
        text(indGroup, 0, num2str(p));
    end
    maxpoint = max(cat(2, plot_data{1}, plot_data{2}));
    minpoint = min(cat(2, plot_data{1}, plot_data{2}));
    xlim([.25 2.75]); ylim([minpoint-0.1*(maxpoint-minpoint) maxpoint+0.1*(maxpoint-minpoint)])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel([parameters{indParam},' linear'])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    [h2, p] = ttest2(plot_data{1}, plot_data{2}, 'Vartype', 'unequal'); % Welch's t-test
    title(['p = ', num2str(p)])
end

figureName = 'f02_LV1_wscentered';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

% plot intercept by group
parameters = {'TFR_theta_int', 'TFR_alpha_int', 'TFR_gamma_int',...
    'sampen_int', 'OneOverF_int', 'pupil_int', 'all_int', 'all_int_equalized'};
colorm = repmat([.6 .6 .6],8,1);

h = figure('units','normalized','position',[.1 .1 .5 .2]);
for indParam = 1:8
    subplot(2,4,indParam)
    curParam = BS.(parameters{indParam});
    plot_data{1} = curParam(idx_YA);
    plot_data{2} = curParam(idx_OA);
    set(gcf,'renderer','Painters')
    cla;
    hold on;
    for indGroup = 1:2
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(indParam,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
        % plot individual values on top
        scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
            plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
        % test deviation from zero
        [h1, p] = ttest(plot_data{indGroup});
        text(indGroup, 0, num2str(p));
    end
    xlim([.25 2.75]); %ylim([-2 2])
    set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); xlabel('Age Group'); ylabel([parameters{indParam},' linear'])
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    [h2, p] = ttest2(plot_data{1}, plot_data{2}, 'Vartype', 'unequal'); % Welch's t-test
    title(['p = ', num2str(p)])
end

%% export equalized linear brainscore changes for further analysis

    LV1.linear = BS.all_equalized;
    LV1.IDs = IDs;
    
    save([pn.root, 'B_analyses/S2B_TFR_v6/B_data/K14_LV1_eq.mat'], 'LV1')

%% intercept-slope correlation

figure;
hold on;
    scatter(BS.all_int_equalized(idx_YA), BS.all_equalized(idx_YA), 'filled', 'FillColor', 'k')
    scatter(BS.all_int_equalized(idx_OA), BS.all_equalized(idx_OA), 'filled', 'FillColor', 'r')
[r, p] = corrcoef(BS.all_int_equalized, BS.all_equalized)
title("Intercept-change correlation all")
xlabel("Intercept")
ylabel("Linear load coefficient")
text(2,0,['r = ', num2str(round(r(2),2))])
set(findall(gcf,'-property','FontSize'),'FontSize',20)
% correlation of -.5 across groups

%     BS.all_equalized_L1234 = cat(3, BS.TFR_theta, ...
%     BS.TFR_alpha, ...
%     BS.TFR_gamma, ...
%     BS.sampen, ...
%     BS.OneOverF, ...
%     BS.pupil);
%     BS.all_equalized_L1234 = squeeze(nanmean(BS.all_equalized_L1234,3));
%     
%     h = figure('units','normalized','position',[.1 .1 .2 .2]);
%     for indParam = 1
%         curParam = BS.OneOverF;
%         plot_data{1} = curParam(:,idx_YA);
%         plot_data{2} = curParam(:,idx_OA);
%         set(gcf,'renderer','Painters')
%         cla;
%         hold on;
%         for indGroup = 1:2
%             plot([1:4], nanmean(plot_data{indGroup},2));
%         end
%         set(findall(gcf,'-property','FontSize'),'FontSize',20)
%     end

%% create csv table from existing variables

%% statistics within MATLAB

y = BrainScore_TFR_theta(:);
AgeVector = repmat([repmat(0, 1, numel(find(idx_YA))), repmat(1, 1, numel(find(idx_OA)))],4,1);
AgeVector = AgeVector(:);
ID = repmat(1:numel(idx_YA),4,1); ID = ID(:);
LoadVector = repmat([1:4], 1, numel(idx_YA))';

tbl = table(double(y),double(LoadVector),double(AgeVector),double(ID),'VariableNames',{'DepVar','Load','Age', 'ID'});
mdl = fitlme(tbl,'DepVar~Load*Age +(1+Load|ID)')

y = BrainScore_TFR_theta(:);
AgeVector = repmat([repmat(0, 1, numel(find(idx_YA))), repmat(1, 1, numel(find(idx_OA)))],4,1);
AgeVector = AgeVector(:);
LoadVector = repmat([1:4], 1, numel(idx_YA))';

tbl = table(double(y),double(LoadVector),double(AgeVector),double(ID),'VariableNames',{'DepVar','Load','Age', 'ID'});
tbl.DepVar = zscore(tbl.DepVar);
tbl.Load = zscore(tbl.Load);
tbl.Age = tbl.Age;
mdl = fitlme(tbl,'DepVar~Load*Age +(1+Load|ID)')
