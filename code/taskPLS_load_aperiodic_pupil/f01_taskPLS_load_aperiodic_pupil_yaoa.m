% Build a two age-group model, probing joint uncertainty-related changes in
% spectral power, pupil dilation, 1/f exponents, sample entropy

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data             = fullfile(rootpath, 'data');
pn.data_tfr         = fullfile(rootpath, 'data', 'tfrPowBySub');
pn.data_gamma       = fullfile(rootpath, '..', 'tfr_gamma', 'data', 'gammaPowBySub');
pn.data_aperiodic   = fullfile(rootpath, '..', 'aperiodic_fooof', 'data');
pn.data_mse         = fullfile(rootpath, '..', 'mse', 'data', 'mmse_output');
pn.data_pupil       = fullfile(rootpath, '..', '..', 'stsw_eye', 'pupil', 'data', 'A_pupilDiameter_cutByStim');
pn.tools            = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

%% add seed for reproducibility

rng(0, 'twister');

%% load data

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = IDs(cellfun(@str2num, IDs, 'un', 1)<2000);
ageIdx{2} = IDs(cellfun(@str2num, IDs, 'un', 1)>2000);

for indGroup = 1:2
    for indID = 1:numel(ageIdx{indGroup})
        disp(['Processing ID', ageIdx{indGroup}{indID}])
        % load wavelet results
        tmp = load(fullfile(pn.data_tfr, [ageIdx{indGroup}{indID}, '_tfr.mat']), 'load_log10');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_Wavelet = find(tmp.load_log10.time >= 2.5 & tmp.load_log10.time < 6);
            TFRdata_Wavelet{indGroup}(indID,:,:,:,indCond) = tmp.load_log10.powspctrm(indCond,:,:,timeIdx_Wavelet);
        end
        % load Gamma results
        tmp_MTM = load(fullfile(pn.data_gamma, [ageIdx{indGroup}{indID}, '_GammaMTM_Zst_v3.mat']), 'Gamma');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.time >= 2.5 & tmp_MTM.Gamma{1,indCond}.time < 6);
            TFRdata_MTM{indGroup}(indID,:,:,:,indCond) = tmp_MTM.Gamma{1,indCond}.powspctrm(:,:,timeIdx_MTM);
        end
    end
    % concatenate wavelet and multitaper results for PLS (demean each matrix)
    
    % Note: the beta range is removed here to focus on anticipated changes in
    % theta, alpha and gamma range. Including beta-band changes in the solution
    % may create a component that rather loads on anticipatory motor
    % preparation that we want to exclude as a potential confound here.
    % i.e., we regularize the solution somewhat

    TFRdata{indGroup} = cat(3, TFRdata_Wavelet{indGroup}(:,:,1:14,:,:), TFRdata_MTM{indGroup}(:,:,15:end,:,:));
end

TFR_freqs = [tmp.load_log10.freq(1:14), tmp_MTM.Gamma{1}.freq(15:end)];

TFRtime = tmp_MTM.Gamma{1,indCond}.time(tmp_MTM.Gamma{1,indCond}.time >= 2.5 & tmp_MTM.Gamma{1,indCond}.time < 6);

% censor timepoints prior to stim onset to focus solution of stim-related dynamics
TFRdata{1}(:,:,:,TFRtime<3.5,:) = 0;
TFRdata{2}(:,:,:,TFRtime<3.5,:) = 0;

%% attach sample entropy, 1/f slopes

SampEn = load(fullfile(pn.data_mse, 'mseavg.mat'), 'mseavg');

idx_YA = find(ismember(SampEn.mseavg.IDs, ageIdx{1}));
idx_OA = find(ismember(SampEn.mseavg.IDs, ageIdx{2}));

% interpolate data to spectral power time

for indChannel = 1:size(SampEn.mseavg.dat,2)
    for indScale = 1:size(SampEn.mseavg.dat,3)
        for indCond = 1:size(SampEn.mseavg.dat,5)
            for indID = 1:size(SampEn.mseavg.dat,1)
                x = SampEn.mseavg.time; xq = TFRtime;
                v = squeeze(SampEn.mseavg.dat(indID,indChannel,indScale,:,indCond));
                SampEn.mseavg.dat_int(indID,indChannel,indScale,:,indCond) = interp1(x,v,xq);
            end
        end
    end
end

% only include first scale
SE{1} = SampEn;
SE{1}.mseavg.dat = SampEn.mseavg.dat_int(idx_YA,:,1,:,:);
SE{2} = SampEn;
SE{2}.mseavg.dat = SampEn.mseavg.dat_int(idx_OA,:,1,:,:);

% add 1/f slopes
slopes_YA = load(fullfile(pn.data_aperiodic, 'C_SlopeFits_fooof_YA.mat'), 'SlopeFits');
slopes_OA = load(fullfile(pn.data_aperiodic, 'C_SlopeFits_fooof_OA.mat'), 'SlopeFits');

idx_YA = find(ismember(slopes_YA.SlopeFits.IDs, ageIdx{1}));
idx_OA = find(ismember(slopes_OA.SlopeFits.IDs, ageIdx{2}));

slopes_YA.SlopeFits.linFit_2_30 = slopes_YA.SlopeFits.linFit_fooof(idx_YA,:,:);
slopes_OA.SlopeFits.linFit_2_30 = slopes_OA.SlopeFits.linFit_fooof(idx_OA,:,:);

% set fronto-central slopes to zero to regularize solution

slopes_YA.SlopeFits.linFit_2_30(:,:,1:43) = 0;
slopes_OA.SlopeFits.linFit_2_30(:,:,1:43) = 0;

% append slopes values to frequency dimension
slopesYA = repmat(permute(shiftdim(slopes_YA.SlopeFits.linFit_2_30,-2),[3,5,1,2,4]),1,1,1,size(SE{1}.mseavg.dat,4),1);
%slopesYA(:,:,:,2:end,:) = NaN; % replace repmat with NaNs to avoid skewing the solution
slopesOA = repmat(permute(shiftdim(slopes_OA.SlopeFits.linFit_2_30,-2),[3,5,1,2,4]),1,1,1,size(SE{1}.mseavg.dat,4),1);
%slopesOA(:,:,:,2:end,:) = NaN; % replace repmat with NaNs to avoid skewing the solution
SE{1}.mseavg.dat = cat(3, SE{1}.mseavg.dat, slopesYA);
SE{2}.mseavg.dat = cat(3, SE{2}.mseavg.dat, slopesOA);

%% attach pupil derivatives

load(fullfile(pn.data_pupil, 'G1_pupilDataFT.mat'), 'pupilDataFT')

tmp_TFRdata = cat(4, pupilDataFT{1}.dataDeriv, pupilDataFT{2}.dataDeriv, ...
    pupilDataFT{3}.dataDeriv, pupilDataFT{4}.dataDeriv);

idx_YA = find(ismember(cat(1,slopes_YA.SlopeFits.IDs, slopes_OA.SlopeFits.IDs), ageIdx{1}));
idx_OA = find(ismember(cat(1,slopes_YA.SlopeFits.IDs, slopes_OA.SlopeFits.IDs), ageIdx{2}));

PupilData{1} = squeeze(nanmean(tmp_TFRdata,2)); % avg. across trials
PupilData{2} = squeeze(nanmean(tmp_TFRdata,2));

PupilData{1} = cat(1,PupilData{1}, PupilData{2});

PupilTime = [pupilDataFT{1}.time(2:end)];

% interpolate data to spectral power time

for indCond = 1:size(PupilData{1},3)
    for indID = 1:size(PupilData{1},1)
        x = PupilTime; xq = TFRtime;
        v = squeeze(PupilData{1}(indID,:,indCond));
        PupilData_int(indID,:,indCond) = interp1(x,v,xq, 'linear', 'extrap');
    end
end

PupilData = [];
PupilData{1} = PupilData_int(idx_YA,:,:,:);
PupilData{2} = PupilData_int(idx_OA,:,:,:);

%% put everything together

% concatenate variables along the frequency dimension

PupilData_tmp{1} = repmat(permute(shiftdim(PupilData{1},-2),[3,1,2,4,5]),1,60,1,1,1);
PupilData_tmp{2} = repmat(permute(shiftdim(PupilData{2},-2),[3,1,2,4,5]),1,60,1,1,1);

TFRdata{1} = cat(3, TFRdata{1}, SE{1}.mseavg.dat, PupilData_tmp{1});
TFRdata{2} = cat(3, TFRdata{2}, SE{2}.mseavg.dat, PupilData_tmp{2});

TFR_freqs = [TFR_freqs, NaN, NaN, NaN]; % append new pseudodata (e.g., pupil)

% z-score across load levels prior to entering into the analysis
% TFRdata{1} = zscore(TFRdata{1},[],5);
% TFRdata{2} = zscore(TFRdata{2},[],5);

%% build models

%mseavg.dat % sub*chan*scale*time*cond

time = TFRtime;
freq = [tmp.load_log10.freq(1:14), tmp_MTM.Gamma{1,1}.freq(15:end), 1000, 1001, 2000];
elec = tmp.load_log10.elec;

% create datamat

num_chans = size(TFRdata{1},2);
num_freqs = size(TFRdata{1},3);
num_time = size(TFRdata{1},4);

num_subj_lst = [numel(ageIdx{1}), numel(ageIdx{2})];
num_cond = 4;
num_grp = 2;

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = TFRdata{indGroup};
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;
stat.freq = freq;
stat.time = time;

save(fullfile(pn.data, 'f01_pls.mat'), ...
    'stat', 'result', 'lvdat', 'lv_evt_list')
 
save(fullfile(pn.data, 'f01_pls_data.mat'), ...
    'datamat_lst', 'TFR_freqs', 'num_chans', 'num_freqs', 'num_time')
