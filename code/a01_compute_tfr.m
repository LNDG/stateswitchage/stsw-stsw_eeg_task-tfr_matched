function a01_compute_tfr(id, rootpath)

if ismac % run if function is not pre-compiled
    id = '1126'; % test for example subject
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

%% pathdef

pn.dataIn       = fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.out          = fullfile(rootpath, 'data', 'tfrPowBySub');
    if ~exist(pn.out); mkdir(pn.out); end
pn.tools        = fullfile(rootpath, 'tools'); addpath(pn.tools);
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

%% load data

display(['processing ID ' id]);
load(fullfile(pn.dataIn, [id, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']), 'data');

%% CSD transform

TrlInfo = data.TrlInfo;
TrlInfoLabels = data.TrlInfoLabels;

csd_cfg = [];
csd_cfg.elecfile = 'standard_1005.elc';
csd_cfg.method = 'spline';
data = ft_scalpcurrentdensity(csd_cfg, data);
data.TrlInfo = TrlInfo; clear TrlInfo;
data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;

%% wavelets

cfg = [];
cfg.channel     = 'all';
cfg.method      = 'wavelet';
cfg.width       = 7;
cfg.keeptrials  = 'yes';
cfg.output      = 'pow';
cfg.foi         = 2:1:25;
cfg.toi         = -1.5:0.05:9.5;
TFRwave         = ft_freqanalysis(cfg, data);

% add TrlInfo
TFRwave.TrlInfo = data.TrlInfo;
TFRwave.TrlInfoLabels = data.TrlInfoLabels;

TFRwave.dimord = 'rpt_chan_freq_time';

%% average within condition

% single-trial log10 transform, no bl

load_log10 = TFRwave;
load_log10 = rmfield(load_log10, 'powspctrm');
for indCond = 1:4
    load_log10.powspctrm(indCond,:,:,:) = squeeze(nanmean(log10(TFRwave.powspctrm(TFRwave.TrlInfo(:,8)==indCond,:,:,:)),1));
end

%% save average data

save(fullfile(pn.out, [id, '_tfr.mat']), 'load_log10');
                