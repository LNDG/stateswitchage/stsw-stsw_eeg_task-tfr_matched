% Set up EEG PLS for a task PLS using spectral power

% add path to toolbox

clear all; cla; clc;
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/[MEG]PLS/MEGPLS_PIPELINE_v2.02b'))

%% add seed for reproducibility

rng(0, 'twister');

%% main PLS part

% load data and prepare as PLS input

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/';
pn.MTMRoot = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/O_gammaPowBySub/';
addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/T_tools/fieldtrip-20170904/']);
ft_defaults

%% load data

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for indGroup = 1:2
    for indID = 1:numel(IDs{indGroup})
        % load wavelet results
        tmp = load([pn.root, 'B_data/D5_TFRavg_v6/', IDs{indGroup}{indID}, '_dynamic_TFRwavePow_v6.mat'], 'TFRStruct_load_log10');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_Wavelet = find(tmp.TFRStruct_load_log10.time >= 2 & tmp.TFRStruct_load_log10.time < 3);
            TFRdata_Wavelet{indGroup}(indID,:,:,:,indCond) = tmp.TFRStruct_load_log10.powspctrm(indCond,:,:,timeIdx_Wavelet);
        end
        % load Gamma results
        tmp_MTM = load([pn.MTMRoot, IDs{indGroup}{indID}, '_GammaMTM_zscore_v2.mat'], 'Gamma');
        for indCond = 1:4
            % restrict to the same time points
            timeIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.time >= 2 & tmp_MTM.Gamma{1,indCond}.time < 3);
            TFRdata_MTM{indGroup}(indID,:,:,:,indCond) = tmp_MTM.Gamma{1,indCond}.powspctrm2(:,:,timeIdx_MTM);
        end
    end
    % concatenate wavelet and multitaper results for PLS (demean each matrix)
    
    % Note: the beta range is removed here to focus on anticipated changes in
    % theta, alpha and gamma range. Including beta-band changes in the solution
    % may create a component that rather loads on anticipatory motor
    % preparation that we want to exclude as a potential confound here.
    % i.e., we regularize the solution somewhat

    TFRdata{indGroup} = cat(3, TFRdata_Wavelet{indGroup}(:,:,1:14,:,:), TFRdata_MTM{indGroup}(:,:,15:end,:,:));
end

%mseavg.dat % sub*chan*scale*time*cond

time = tmp.TFRStruct_load_log10.time(timeIdx_Wavelet);
freq = [tmp.TFRStruct_load_log10.freq(1:14), tmp_MTM.Gamma{1,1}.freq(15:end)];
elec = tmp.TFRStruct_load_log10.elec;

% create datamat

% timeIdx = find(time > 3.5 & time < 6);
% curTFRdata = TFRdata(:,:,:,timeIdx,:);

num_chans = size(TFRdata{1},2);
num_freqs = size(TFRdata{1},3);
num_time = size(TFRdata{1},4);

num_subj_lst = [numel(IDs{1}), numel(IDs{2})];
num_cond = 4;
num_grp = 2;

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = TFRdata{indGroup};
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;
stat.freq = freq;
stat.time = time;

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/K11_taskPLSPrestim_YA_OA.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')
