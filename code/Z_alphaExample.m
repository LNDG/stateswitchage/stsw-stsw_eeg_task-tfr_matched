% exemplary plots:
% sub 1 trl 10
% sub 37 (1247): large modulation, L1: trl 40, 158; L4: 17
% sub 38: L1: 175
% 3: L1: 160, L4: 213,5, 83
% sub 15: l1: 85, 188 l4: 131 (lots of alpha during stim)
% sub 22: best performer, l1: 61, 98, 154, 155, 160, L4: 125

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/S2_TFR_PeriResponse/T_tools/']; addpath(pn.tools);
pn.savePath     = [pn.root, 'B_analyses/S2B_TFR_v6/B_data/'];
pn.plotFolder   = [pn.root, 'B_analyses/S2B_TFR_v6/C_figures/'];

addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

condEEG = 'dynamic';

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

id = 22;

display(['processing ID ' IDs{id}]);

%% load data

tmp = [];
tmp.clock = tic; % set tic for processing duration

load([pn.History, IDs{id}, '_dynamic_config.mat'],'config');
load([pn.dataIn, IDs{id}, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

%% CSD transform

TrlInfo = data.TrlInfo;
TrlInfoLabels = data.TrlInfoLabels;

csd_cfg = [];
csd_cfg.elecfile = 'standard_1005.elc';
csd_cfg.method = 'spline';
data = ft_scalpcurrentdensity(csd_cfg, data);
data.TrlInfo = TrlInfo; clear TrlInfo;
data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;

%% high-pass filter at 6 Hz

% cfg = [];
% cfg.hpfilter = 'yes';
% cfg.hpfreq = 4;
% 
% cfg.lpfilter = 'yes';
% cfg.lpfreq = 40;
% [data_HP] = ft_preprocessing(cfg, data);
% 
% 
% cfg = [];
% cfg.hpfilter = 'yes';
% cfg.hpfreq = 50;
% [data_Gamma] = ft_preprocessing(cfg, data);

%% extract exemplary single trial alpha signal

h = figure('units','normalized','position',[.1 .1 .7 .2]);
hold on;
patches.timeVec = [0 3];
patches.colorVec = [1 .95 .8];
for indP = 1:size(patches.timeVec,2)-1
    YLim = [-3 3]* 10^-3;
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
patches.timeVec = [-3 -2];
patches.colorVec = [.9 .9 .9];
for indP = 1:size(patches.timeVec,2)-1
    YLim = [-3 3]* 10^-3;
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
plot(data.time{1}-3,squeeze(nanmean(data.trial{73}(58:60,:),1)), 'Color', 'k');
xlim([-4 5])
xlabel('Time from stimulus onset (s)');
ylabel('Amplitude (microV)')
title('Posterior signal across a single exemplary trial')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/C_figures/';
figureName = 'Z_exampleAlpha1';

saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%figure; plot(data_HP.time{1},squeeze(nanmean(data_HP.trial{40}(50:60,:),1)));

%% experiment

h = figure('units','normalized','position',[.1 .1 .7 .2]);
for indTrial = 1:100
    cla; hold on;
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-3 3]* 10^-3;
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    patches.timeVec = [-3 -2];
    patches.colorVec = [.9 .9 .9];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-3 3]* 10^-3;
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end

    plot(data.time{1}-3,squeeze(nanmean(data.trial{indTrial}(58:60,:),1)), 'Color', 'k');
    xlim([-4 5])
    xlabel('Time from stimulus onset (s)');
    ylabel('Amplitude (microV)')
    title(num2str(indTrial))
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pause(1)
end

%% randomly draw a trial from load 1 and load 4

eligible_1 = find(data.TrlInfo(:,8)==1);
eligible_4 = find(data.TrlInfo(:,8)==4);

draw1 = randperm(numel(eligible_1));
trl_1 = eligible_1(draw1(1));

draw4 = randperm(numel(eligible_4));
trl_4 = eligible_4(draw4(1));

% trl_1 = 58;
% trl_1 = 57;
% trl_1 = 102;
%trl_1 = 134;
%trl_1 = 135;
%trl_1 = 179;
%trl_1 = 159;

% trl_4 = 124;
% trl_4 = 33;

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(2,1,1);cla; hold on;
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-3 3]* 10^-3;
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    patches.timeVec = [-3 -2];
    patches.colorVec = [.9 .9 .9];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-3 3]* 10^-3;
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    plot(data.time{1}-3,squeeze(nanmedian(data.trial{trl_1}(58:60,:),1)), 'Color', 'k');
    %plot(data.time{1}-3,squeeze(nanmean(data.trial{trl_1}(44:60,:),1)), 'Color', 'r');
    xlim([-4 5])
    xlabel('Time from stimulus onset (s)');
    ylabel('Amplitude (microV)')
    title('Example trial, single target, correct response, RT = .31 s')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(2,1,2);cla; hold on;
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-3 3]* 10^-3;
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    patches.timeVec = [-3 -2];
    patches.colorVec = [.9 .9 .9];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-3 3]* 10^-3;
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    plot(data.time{1}-3,squeeze(nanmedian(data.trial{trl_4}(58:60,:),1)), 'Color', 'k');
    %plot(data.time{1}-3,squeeze(nanmean(data.trial{trl_4}(44:60,:),1)), 'Color', 'r');
    xlim([-4 5])
    xlabel('Time from stimulus onset (s)');
    ylabel('Amplitude (microV)')
    title('Example trial, four targets, correct response, RT = 1.07 s')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/C_figures/';
figureName = 'Z_exampleAlpha2';

saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
    
 % 154, 57, 58 61, 102, 179
% 124, 33, (148, 32)
h = figure('units','normalized','position',[.1 .1 .7 .4]);
for indRand = 1:100
    eligible_1 = find(data.TrlInfo(:,8)==1);
    eligible_4 = find(data.TrlInfo(:,8)==4);

    draw1 = randperm(numel(eligible_1));
    trl_1 = eligible_1(draw1(1));

    draw4 = randperm(numel(eligible_4));
    trl_4 = eligible_4(draw4(1));

    subplot(2,1,1);cla; hold on;
        patches.timeVec = [0 3];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-3 3]* 10^-3;
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        patches.timeVec = [-3 -2];
        patches.colorVec = [.9 .9 .9];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-3 3]* 10^-3;
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        %plot(data.time{1}-3,squeeze(nanmean(data.trial{trl_1}(58:60,:),1))-squeeze(nanmean(data.trial{trl_1}(40:57,:),1)), 'Color', 'k');
        plot(data.time{1}-3,squeeze(nanmean(data.trial{trl_1}(55:60,:),1)), 'Color', 'k');
        xlim([-4 5])
        xlabel('Time from stimulus onset (s)');
        ylabel('Amplitude (microV)')
        title(num2str(trl_1))
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
    subplot(2,1,2);cla; hold on;
        patches.timeVec = [0 3];
        patches.colorVec = [1 .95 .8];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-3 3]* 10^-3;
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        patches.timeVec = [-3 -2];
        patches.colorVec = [.9 .9 .9];
        for indP = 1:size(patches.timeVec,2)-1
            YLim = [-3 3]* 10^-3;
            p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                        [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
            p.EdgeColor = 'none';
        end
        %plot(data.time{1}-3,squeeze(nanmean(data.trial{trl_4}(58:60,:),1))-squeeze(nanmean(data.trial{trl_4}(40:57,:),1)), 'Color', 'k');
        plot(data.time{1}-3,squeeze(nanmean(data.trial{trl_4}(55:60,:),1)), 'Color', 'k');
        xlim([-4 5])
        xlabel('Time from stimulus onset (s)');
        ylabel('Amplitude (microV)')
        title(num2str(trl_4))
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        pause(2)
end