% visual temporal location of load effects for diverse measures

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data         = fullfile(pn.root, 'data');    
pn.tools        = fullfile(pn.root, 'tools'); addpath(pn.tools);
pn.figures      = fullfile(pn.root, 'figures');    
pn.tfrdata      = fullfile(pn.root, 'data', 'tfrPowBySub');
pn.gammadata	= fullfile(pn.root, '..', 'tfr_gamma', 'data', 'gammaPowBySub');

addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));
addpath(fullfile(pn.tools, 'shadedErrorBar'));

load(fullfile(pn.data, 'c03_LV1.mat'), 'LV1')

IDs = LV1.IDs;

ageIdx{1} = find(cellfun(@str2num, IDs, 'un', 1)<2000);
ageIdx{2} = find(cellfun(@str2num, IDs, 'un', 1)>2000);

% extract same channels as visualized for PLS solution

idxChanGamma = [53:55, 58:60];

%% load spectral power for theta, alpha and gamma range

for indID = 1:numel(LV1.IDs)
    disp(['Processing ID ', LV1.IDs{indID}])
    % load Gamma results
    tmp_MTM = load(fullfile(pn.gammadata, [LV1.IDs{indID}, '_GammaMTM_Zst_v3.mat']), 'Gamma');
    for indCond = 1:4
        % restrict to the same time points
        timeIdx_MTM = find(tmp_MTM.Gamma{1,indCond}.time >= 1 & tmp_MTM.Gamma{1,indCond}.time < 8);
        TFRdata_MTM(indID,:,:,:,indCond) = tmp_MTM.Gamma{1,indCond}.powspctrm(:,:,timeIdx_MTM);
    end
end

TFRdata = TFRdata_MTM;

time = tmp_MTM.Gamma{1,indCond}.time(timeIdx_MTM)-3;
freq = tmp_MTM.Gamma{1,indCond}.freq;
%elec = tmp_MTM.Gamma{1,indCond}.elec;

gammaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanGamma, freq> 60 & freq <150, :,:),3),2)),[1,3,2]);
%gammaMerged = permute(squeeze(nanmean(nanmean(TFRdata(:,idxChanGamma, freq> 20 & freq <40, :,:),3),2)),[1,3,2]);
% Note that the 20-40Hz range is contaminated by baselined pre-stimulus alpha power increases

colGrey = brewermap(4,'Greys');
colBlue = brewermap(4,'Reds');

%% md split on LV1: YA

[~, sortInd_LV1] = sort(LV1.linear(ageIdx{1}),'ascend');
sortInd_LV1_bottom = sortInd_LV1(1:ceil(numel(sortInd_LV1)/2));
sortInd_LV1_top = sortInd_LV1(ceil(numel(sortInd_LV1)/2)+1:end);

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    curIDs = ageIdx{1}(sortInd_LV1_top);
    grandAverage = squeeze(nanmean(gammaMerged(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(gammaMerged(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,2,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,3,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    legend([l1.mainLine, l4.mainLine], {'Load 1'; 'Load 4'}, 'location', 'NorthWest'); legend('boxoff')
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); %ylim([-.03 .2])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('LV1 top');
    
subplot(1,2,2);
    cla; hold on;
    curIDs = ageIdx{1}(sortInd_LV1_bottom);
    grandAverage = squeeze(nanmean(gammaMerged(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(gammaMerged(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,2,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,3,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); %ylim([-.03 .2])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('LV1 bottom');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% md split on LV1: OA

[~, sortInd_LV1] = sort(LV1.linear(ageIdx{2}),'ascend');
sortInd_LV1_bottom = sortInd_LV1(1:ceil(numel(sortInd_LV1)/2));
sortInd_LV1_top = sortInd_LV1(ceil(numel(sortInd_LV1)/2)+1:end);

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    curIDs = ageIdx{2}(sortInd_LV1_top);
    grandAverage = squeeze(nanmean(gammaMerged(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(gammaMerged(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,2,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,3,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    legend([l1.mainLine, l4.mainLine], {'Load 1'; 'Load 4'}, 'location', 'NorthWest'); legend('boxoff')
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); %ylim([-.03 .2])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('LV1 top');
    
subplot(1,2,2);
    cla; hold on;
    curIDs = ageIdx{2}(sortInd_LV1_bottom);
    grandAverage = squeeze(nanmean(gammaMerged(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(gammaMerged(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,2,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,3,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(gammaMerged(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    xlabel('Time (ms); response-locked')
    xlim([-2 5]); %ylim([-.03 .2])
    ylabel({'40-90 Hz Gamma Power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('LV1 bottom');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

