% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools	= [pn.root, 'B_analyses/S2B_TFR_v6/T_tools/']; addpath(pn.tools);
addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/T_tools/fieldtrip-20170904/']); ft_defaults;
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
pn.functions    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/T_tools/'; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]); % requires mysigstar_vert!
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/RainCloudPlots'))

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/C_figures/';

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)


load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/K16_behavPLSStim_YA_OA_2Group.mat', ...
    'stat', 'result', 'lvdat', 'lv_evt_list')

    
indLV = 1;

lvdat = reshape(result.boot_result.compare_u(:,indLV), size(stat.prob,1), size(stat.prob,2), size(stat.prob,3));
stat.prob = lvdat;
stat.mask = lvdat > 3 | lvdat < -3;

%% invert solution

% stat.mask = stat.mask;
% stat.prob = stat.prob.*-1;
% result.vsc = result.vsc.*-1;
% result.usc = result.usc.*-1;

%% plot pupil, 1/f separately

h = figure('units','normalized','position',[.1 .1 .7 .3]);
set(gcf,'renderer','Painters')

% plot multivariate brainscores

maskNaN = double(stat.mask);
maskNaN(maskNaN==0) = NaN;

subplot(2,3,1); cla;
statsPlot = squeeze(nanmean(stat.prob([1],end,:),1));
plot(stat.time-3,statsPlot, 'k')
hold on;
statsMask = squeeze(nanmean(maskNaN([1],end,:),1));
statsMask(statsMask==0) = NaN; statsMask(statsMask>0) = 1;
statsPlot = statsMask.*statsPlot;
plot(stat.time-3,statsPlot, 'r', 'LineWidth', 2)
set(gca,'Ydir','Normal');
xlabel('Time [s from stim onset]'); ylabel('BSR');
title({'Pupil dilation'})

subplot(2,3,2); cla;
statsPlot = squeeze(nanmean(stat.prob(:,end-1,1),3));
plot([1:60],statsPlot, 'k')
hold on;
statsMask = squeeze(nanmean(maskNaN(:,end-1,1),3));
statsMask(statsMask==0) = NaN; statsMask(statsMask>0) = 1;
statsPlot = statsMask.*statsPlot;
scatter([1:60],statsPlot, 'filled', 'r', 'LineWidth', 2)
set(gca,'Ydir','Normal');
xlabel('Channels'); ylabel('BSR');
title({'1/f'})

subplot(2,3,3); cla;
statsPlot = squeeze(nanmean(stat.prob([58:60],end-2,:),1));
plot(stat.time-3,statsPlot, 'k')
hold on;
statsMask = squeeze(nanmean(maskNaN([58:60],end-2,:),1));
statsMask(statsMask==0) = NaN; statsMask(statsMask>0) = 1;
statsPlot = statsMask.*statsPlot;
plot(stat.time-3,statsPlot, 'r', 'LineWidth', 2)
set(gca,'Ydir','Normal');
xlabel('Time [s from stim onset]'); ylabel('BSR');
title({'SampleEntropy'})

% plot multivariate topographies

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
% subplot(2,3,4); cla;
%     cfg.marker = 'off'; 
%     cfg.highlight = 'yes';
%     cfg.highlightchannel = plotData.label(58:60);
%     cfg.highlightcolor = [0 0 0];
%     cfg.highlightsymbol = '.';
%     cfg.highlightsize = 18;
%     cfg.zlim = [-3 3]; 
%     plotData.powspctrm = squeeze(nanmean(nanmean(stat.mask(:,end,:).*stat.prob(:,end,:),2),3)); ft_topoplotER(cfg,plotData);
%     cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
subplot(2,3,5); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(58:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3]; 
    plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,end-1,1).*stat.prob(:,end-1,1),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
subplot(2,3,6); cla;
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label([58:60]);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3]; 
    plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,end-2,:).*stat.prob(:,end-2,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
colormap(cBrew)

%% initiate figure

h = figure('units','normalized','position',[.1 .1 .7 .3]);
set(gcf,'renderer','Painters')

%% plot multivariate brainscores

% maskNaN = double(stat.mask);
% maskNaN(maskNaN==0) = NaN;

subplot(3,3,[2,5,8]);
statsPlot = [];
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(10:12,1:7,:).*stat.prob(10:12,1:7,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask(44:60,8:14,:).*stat.prob(44:60,8:14,:),1)));
statsPlot = cat(1, statsPlot,NaN(1,size(stat.mask,3)));
statsPlot = cat(1, statsPlot,squeeze(nanmean(stat.mask([53:55, 58:60],15:end-3,:).*stat.prob([53:55, 58:60],15:end-3,:),1)));
imagesc(stat.time-3,[],statsPlot,[-4 4])
line([3.5 6]-3, [8 8],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
line([3.5 6]-3, [16 16],'Color', 'k', 'LineStyle', '-', 'LineWidth', 10)
set(gca,'Ydir','Normal');
frequencies = [stat.freq(1:7), NaN, stat.freq(8:14), NaN, stat.freq(15:end-3)];
set(gca, 'YTickLabels', round(frequencies(get(gca, 'YTick')),2));
xlabel('Time [s from stim onset]'); ylabel('Frequency [Hz]');
title({'Multivariate spectral changes'})
cb = colorbar('location', 'EastOutside'); set(get(cb,'title'),'string','Mean BSR');

%% plot multivariate topographies

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
subplot(3,3,[3]); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(10:12);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-3 3]; plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,1:7,:).*stat.prob(:,1:7,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');
subplot(3,3,[6]); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(44:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-5 5]; plotData.powspctrm = squeeze(nanmean(nanmin(stat.mask(:,10:15,:).*stat.prob(:,10:15,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Min BSR');
subplot(3,3,[9]); cla;
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label([53:55, 58:60]);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-.5 .5]; plotData.powspctrm = squeeze(nanmean(nanmax(stat.mask(:,25:end-3,:).*stat.prob(:,25:end-3,:),[],2),3)); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');

%% plot using raincloud plot

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;
    conds = {'Load 1'};
    condData = []; uData = [];
    for indGroup = 1:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
        end
    end
    
    cBrew = brewermap(4,'RdBu');
    cBrew = cBrew([1,4],:);
    
    cBrew(1,:) = 2.*[.3 .1 .1];
    cBrew(2,:) = 2.*[.1 .1 .3];
    
    %% reproduce correlation plots

    pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
    load([pn.dataOut, 'STSWD_summary_YAOA.mat'], 'STSWD_summary')

    idxYA = ismember(STSWD_summary.IDs, IDs{1});
    idxOA = ismember(STSWD_summary.IDs, IDs{2});

    h = figure('units','normalized','position',[.1 .1 .3 .25]);
    set(gcf,'renderer','painters')
    % add AMF
    ax{1} = subplot(2,3,1); cla; hold on;
        y = STSWD_summary.EEGpupil_v2.all_equalized;
        scatter(uData{1}, y(idxYA), 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        scatter(uData{2}, y(idxOA), 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]);l2 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
        %[r, p, RL, RU] = corrcoef(x, y);
        %title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(RL(2),2)),',',num2str(round(RU(2),2)),']'])
        ylabel({'EEGpupil BS';'(linear mod.)'}); xlabel('Brainscore (arb.u.)')
%         ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
%         xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    % add drift
    ax{2} = subplot(2,3,2); cla; hold on;
        y = STSWD_summary.HDDM_vt.driftEEG(:,1);
        scatter(uData{1}, y(idxYA), 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        scatter(uData{2}, y(idxOA), 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]);l2 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
        ylabel({'Drift Load 1'}); xlabel('Brainscore (arb.u.)')
    ax{2} = subplot(2,3,3); cla; hold on;
        y = STSWD_summary.HDDM_vt.driftEEG_linear(:,1);
        scatter(uData{1}, y(idxYA), 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
        scatter(uData{2}, y(idxOA), 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]);l2 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
        ylabel({'Drift Load 1'}); xlabel('Brainscore (arb.u.)')
        
        [r, p, RL, RU] = corrcoef(uData{1}, y(idxYA))
        [r, p, RL, RU] = corrcoef(uData{2}, y(idxOA))
