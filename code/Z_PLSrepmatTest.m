% Set up EEG PLS for a task PLS using spectral power

% add path to toolbox

clear all; cla; clc;
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/[MEG]PLS/MEGPLS_PIPELINE_v2.02b'))

%% add seed for reproducibility

rng(0, 'twister');

repeatChans = 1:5:60;

for indRand = 1:numel(repeatChans)

    %% create random data

    % 50 subjects
    % 6 channels
    % 1 time point
    % 4 conditions

    TFRStruct = rand(50,6,1,1,4);

    %% add varying amounts of repeated data with clear condition differences

    % 1 to 60 channels

    systematicIncrease = cat(5,rand(50,1,1,1,1), 2.*rand(50,1,1,1,1), 3.*rand(50,1,1,1,1), 4.*rand(50,1,1,1,1));

    TFRStruct = cat(2,TFRStruct, repmat(systematicIncrease,1,repeatChans(indRand),1,1,1));

    % create datamat

    num_chans = size(TFRStruct,2);
    num_freqs = size(TFRStruct,3);
    num_time = size(TFRStruct,4);

    num_subj_lst = 50;
    num_cond = 4;
    num_grp = 1;

    datamat_lst = cell(num_grp); lv_evt_list = [];
    indCount_cont = 1;
    for indGroup = 1:num_grp
        indCount = 1;
        curTFRdata = TFRStruct;
        for indCond = 1:num_cond
            for indID = 1:num_subj_lst(indGroup)
                datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
                lv_evt_list(indCount_cont) = indCond;
                indCount = indCount+1;
                indCount_cont = indCount_cont+1;
            end
        end
    end
    datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

    %% set PLS options and run PLS

    option = [];
    option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
    option.num_perm = 1000; %( single non-negative integer )
    option.num_split = 0; %( single non-negative integer )
    option.num_boot = 1000; % ( single non-negative integer )
    option.cormode = 0; % [0] | 2 | 4 | 6
    option.meancentering_type = 0;% [0] | 1 | 2 | 3
    option.boot_type = 'strat'; %['strat'] | 'nonstrat'

    result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

    %% rearrange into fieldtrip structure

    lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
    %udat = reshape(result.u, num_chans, num_freqs, num_time);
    
    Results.p(indRand) = result.perm_result.sprob(1);

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;
    conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
    condData = []; uData = [];
    for indGroup = 1%:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
        end
    end
    
    Results.BS(indRand,:,:) = uData{1}';

end


figure; plot(Results.p)

figure; 
imagesc([1:4],repeatChans,squeeze(nanmean(abs(Results.BS),2)))
xlabel('Condition'); ylabel('Number of repmat with condition effect')

%% 2nd test case: attach NaNs

rng(0, 'twister');

repeatChans = 1:5:60;

for indRand = 1:numel(repeatChans)

    %% create random data

    % 50 subjects
    % 6 channels
    % 1 time point
    % 4 conditions

    TFRStruct = rand(50,6,1,1,4);

    %% add varying amounts of repeated data with clear condition differences

    % 1 to 60 channels

    systematicIncrease = cat(5,rand(50,1,1,1,1), 2.*rand(50,1,1,1,1), 3.*rand(50,1,1,1,1), 4.*rand(50,1,1,1,1));

    TFRStruct = cat(2,TFRStruct, systematicIncrease, repmat(NaN(size(systematicIncrease)),1,repeatChans(indRand),1,1,1));

    % create datamat

    num_chans = size(TFRStruct,2);
    num_freqs = size(TFRStruct,3);
    num_time = size(TFRStruct,4);

    num_subj_lst = 50;
    num_cond = 4;
    num_grp = 1;

    datamat_lst = cell(num_grp); lv_evt_list = [];
    indCount_cont = 1;
    for indGroup = 1:num_grp
        indCount = 1;
        curTFRdata = TFRStruct;
        for indCond = 1:num_cond
            for indID = 1:num_subj_lst(indGroup)
                datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
                lv_evt_list(indCount_cont) = indCond;
                indCount = indCount+1;
                indCount_cont = indCount_cont+1;
            end
        end
    end
    datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

    %% set PLS options and run PLS

    option = [];
    option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
    option.num_perm = 1000; %( single non-negative integer )
    option.num_split = 0; %( single non-negative integer )
    option.num_boot = 1000; % ( single non-negative integer )
    option.cormode = 0; % [0] | 2 | 4 | 6
    option.meancentering_type = 0;% [0] | 1 | 2 | 3
    option.boot_type = 'strat'; %['strat'] | 'nonstrat'

    result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

    %% rearrange into fieldtrip structure

    lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
    %udat = reshape(result.u, num_chans, num_freqs, num_time);
    
    Results.p(indRand) = result.perm_result.sprob(1);

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;
    conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
    condData = []; uData = [];
    for indGroup = 1%:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
        end
    end
    
    Results.BS(indRand,:,:) = uData{1}';

end


figure; plot(Results.p)

figure; 
imagesc([1:4],repeatChans,squeeze(nanmean(abs(Results.BS),2)))
xlabel('Condition'); ylabel('Number of repmat with condition effect')
title('Fill remaining dimensions with NaNs instead of repmat')

%% 3rd test case: only 1 channel, but either repmat or NaN-filled

clear Results;

rng(0, 'twister');

for indRand = 1:2

    %% create random data

    % 50 subjects
    % 6 channels
    % 1 time point
    % 4 conditions

    TFRStruct = rand(50,6,1,10,4);

    %% add varying amounts of repeated data with clear condition differences

    % 1 to 60 channels

    systematicIncrease = cat(5,rand(50,1,1,10,1), 2.*rand(50,1,1,10,1), 3.*rand(50,1,1,10,1), 4.*rand(50,1,1,10,1));

    if indRand == 1
        systematicIncrease(:,:,:,2:end,:) = NaN;
    end
    
    TFRStruct = cat(2,TFRStruct, systematicIncrease);

    % create datamat

    num_chans = size(TFRStruct,2);
    num_freqs = size(TFRStruct,3);
    num_time = size(TFRStruct,4);

    num_subj_lst = 50;
    num_cond = 4;
    num_grp = 1;

    datamat_lst = cell(num_grp); lv_evt_list = [];
    indCount_cont = 1;
    for indGroup = 1:num_grp
        indCount = 1;
        curTFRdata = TFRStruct;
        for indCond = 1:num_cond
            for indID = 1:num_subj_lst(indGroup)
                datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
                lv_evt_list(indCount_cont) = indCond;
                indCount = indCount+1;
                indCount_cont = indCount_cont+1;
            end
        end
    end
    datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

    %% set PLS options and run PLS

    option = [];
    option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
    option.num_perm = 1000; %( single non-negative integer )
    option.num_split = 0; %( single non-negative integer )
    option.num_boot = 1000; % ( single non-negative integer )
    option.cormode = 0; % [0] | 2 | 4 | 6
    option.meancentering_type = 0;% [0] | 1 | 2 | 3
    option.boot_type = 'strat'; %['strat'] | 'nonstrat'

    result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

    %% rearrange into fieldtrip structure

    lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
    %udat = reshape(result.u, num_chans, num_freqs, num_time);
    
    Results.p(indRand) = result.perm_result.sprob(1);

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;
    conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
    condData = []; uData = [];
    for indGroup = 1%:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
        end
    end
    
    Results.BS(indRand,:,:) = uData{1}';

end


figure; plot(Results.p)

figure; 
imagesc([1:4],repeatChans,squeeze(nanmean(abs(Results.BS),2)))
xlabel('Condition'); ylabel('Number of repmat with condition effect')
